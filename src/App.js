import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';
import { auth, provider } from './firebase';
import Database from './Database';

class App extends Component {
  state = {
    user: null
  }

  handleLogin = () => auth.signInWithPopup(provider).then(({ user }) => this.setState({ user }))
  handleLogout = () => auth.signOut().then(() => this.setState({ user: null }))

  componentWillMount() {
    auth.onAuthStateChanged(user => this.setState({user}))
  }

  render() {
    if (null === this.state.user) {
      return <LoginScreen onClick={this.handleLogin} />
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
          <button onClick={this.handleLogout}>Logout</button>
        </div>
        <hr />
        <div>
          <Database />
        </div>
      </div>
    );
  }
}

const LoginScreen = (props) => {
  return (
    <div className="App">
      <p className="App-intro">
        <button onClick={props.onClick}>Login</button>
      </p>
    </div>
  )
}
LoginScreen.propTypes = {
  onClick: PropTypes.func.isRequired
}


export default App;
