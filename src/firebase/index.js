import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyAavfzlOGdOfd-F5sRT7qLUQEoCfh2y1Fw",
    authDomain: "fir-592d5.firebaseapp.com",
    databaseURL: "https://fir-592d5.firebaseio.com",
    projectId: "fir-592d5",
    storageBucket: "fir-592d5.appspot.com",
    messagingSenderId: "361021568794"
})

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();

export default firebase;